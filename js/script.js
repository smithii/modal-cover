(function(){
	"use strict";

	function modal(){
      
	    var init = function () {
	        var buttons = document.querySelectorAll(".btn");
	        var modals = document.querySelectorAll(".modal");

	        for (var i = 0; i < buttons.length; i++) {
	            var button = buttons[i];
	            button.onclick = toggle;
	        }

	        for (var i = 0; i < modals.length; i++) {
	            var modal = modals[i];
	            modal.onclick = toggle;
	        }
	    };
      
      var toggle = function (e) {
          var button = e.target;
          var modal = button.getAttribute("data-modal");
          document.getElementById(modal).classList.toggle("open");
      };
      
      return {
        init: init
      };
      
    }
    
    var modal = modal();
    modal.init();
  
})();

